## Manuel Romero Matos

### Hostal d'en Sol, nº8
### 04 de mayo 1983 H2
### 684136835

### 36579859s@iespoblenou.org

[![mi foto](https://i.pinimg.com/236x/04/41/90/044190fd98f2db511efcb02f6a0f4121.jpg)]

## INFORMACIÓN ACADEMICA


- 2014 Auxiliar de montage y mantenimiento de sistemas microinformaticos.

- 2018-2019 Grado Medio Sistemas Microinformaticos y Redes



## FORMACION COMPLEMENTARIA

- 2013 Reparacion de moviles
- 2018-2019 Paginas web en HTML

## IDIOMA
- Catalan: nivel alto tanto escrito como hablado
- Ingles: nivel medio. Escrito y hablado


